# txidio

txidio

## License

Copyright (C) 2021  txidio

Licensed under the MIT license

### Donate

Donate to 3K1oFZMks41C7qDYBsr72SYjapLqDuSYuN to see more development! For https://github.com/txid/coinbin/tree/txid_v2

#### OpenStore

<a href="https://open-store.io/app/txidio.txidio"><img src="https://open-store.io/badges/en_US.png" alt="OpenStore" /></a>
